import { Button, Text, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import React, { Component } from 'react';
import { styles } from './style';

const { container, txt, btn, txtWhite, txtRed } = styles;
export default class BindingData extends Component {
    constructor(props){
        super(props);
        this.state={
            isLogin:false
        }
    }
    textRender = 'Hello React Native 04';
    textLogin = 'Should Login My App';
    renderLogin = () => {
        return (
            <Text style={txt}>{this.state.isLogin ? this.textRender : this.textLogin}</Text>
        )
    }
 
    onPressLogin = () => {
        console.log("LOGIN PRESSED");  
        this.setState({isLogin:true},()=>{console.log('Done',this.state.isLogin);})
    }

    onPressLogout = ()=>{
        console.log("LOGOUT PRESSED");
        this.setState({isLogin:false})
    }
    
    render() { 
        console.log(this.textRender);
        return (
            <View style={container}>
                <Text style={txt}>Binding Data Component</Text>
                {this.renderLogin()}
                {this.state.isLogin ? (<Text style={txt}>
                    {this.textRender}</Text>)
                    : <Text style={txt}>{this.textLogin}</Text>
                }
                {/* <Button title={'Login'} color='green' /> */}

                <TouchableOpacity style={btn} onPress={()=>this.onPressLogin(true)}>
                    <Text style={txtWhite}>LOGIN</Text>
                </TouchableOpacity>
                <TouchableHighlight style={btn} onPress={()=>this.onPressLogout(false)}
                    underlayColor={'blue'}
                >
                    <Text style={txtWhite}>LOGOUT</Text>
                </TouchableHighlight>
                {/* <TouchableWithoutFeedback
                    style={txtRed}
                >
                    <Text >LOGOUT</Text>
                </TouchableWithoutFeedback> */}
            </View>
        );
    }
}
