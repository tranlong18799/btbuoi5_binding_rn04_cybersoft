import { Image, Text, View } from 'react-native';
import React, { Component } from 'react';
import Alto_icon from './images/Alto_icon.png';
import Alto_0 from './images/Alto_0.png';
import { styles } from './style';

const { container, card,cardImage } = styles;
export default class ExamBinding extends Component {
  game = {
    id: 0,
    title: "Alto's Odyssey",
    icon: Alto_icon,
    subTitle:
      'Just beyond the horizon sits a majestic desert, vast and unexplored.',
    description:
      'Just beyond the horizon sits a majestic desert, vast and unexplored.\nJoin Alto and his friends and set off on an endless sandboarding journey to discover its secrets. Soar above windswept dunes, traverse thrilling canyons, and explore long-hidden temples in a fantastical place far from home.',
    age: '9+',
    rating: 4.4,
    banner: Alto_0,
    backgroundColor: '#824671CC'
  }
  render() {

    return (
        <View style={container}>
          <Image source={this.game.banner} resizeMode='contain' />
          <View style={{...card,backgroundColor:this.game.backgroundColor}}>
            <Image source={this.game.icon} style={cardImage} />
            <View style={{flexBasis:'80%'}}>
              <Text style={{color:'#fff',fontSize:20,fontWeight:'700'}}>
                {this.game.title}
              </Text>
              <Text style={{color:'#fff',fontSize:14}}>
                {this.game.subTitle}
              </Text>
            </View>
          </View>
        </View>
    );
  }
}
