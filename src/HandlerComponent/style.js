import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'#343434'
    },
    txt: {
        fontSize: 24,
        fontWeight: '700'
    },
    txtRed: {
        color: 'red'
    },
    txtWhite:{
        color:'white'
    },
    btn: {
        backgroundColor: 'green',
        padding:12,
        color:'white',
        borderRadius:8,
        marginVertical:10
    },
    card:{
        width:'90%',
        marginTop:8,
        padding:10,
        height:100,
        borderRadius:10,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    cardImage:{
        width:50,
        height:50,
        borderRadius:8,
        borderWidth:1,
        borderColor:'rgba(255,255,255,0.5)'
    }
});
