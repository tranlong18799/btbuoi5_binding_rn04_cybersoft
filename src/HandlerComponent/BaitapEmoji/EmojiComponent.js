import { Image, Text, TouchableOpacity, View } from 'react-native';
import React, { Component } from 'react';
import { styles } from './style';
// import angrys from './emoji/angry.png';
// import care from './emoji/care.png';
// import haha from './emoji/haha.png';
// import like from './emoji/like.png'; 
// import love from './emoji/love.png';
// import sad from './emoji/sad.png';
import care from '../images/emoji/care.png';
import angry from '../images/emoji/angry.png';
import haha from '../images/emoji/haha.png';
import like from '../images/emoji/like.png';
import sad from '../images/emoji/sad.png';
import love from '../images/emoji/love.png';

const { container, textLg, imgMain, flexRow, imgChild } = styles;
const emojiArray = [care, angry, haha, like, sad, love];
export default class EmojiComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { emoji: care }
    }

    onPressEmoji = (emoji) => {
        this.setState({ ...this.state, emoji: emoji })
    }

    render() {
        const { emoji } = this.state;
        return (
            <View style={container}>
                <Text style={textLg}>Bạn đang cảm thấy như thế nào?</Text>
                <Image source={emoji} style={imgMain} />
                <View style={flexRow}>
                    {emojiArray?.map((emoji, i) => {
                        return (
                            <TouchableOpacity 
                            key={i} 
                            onPress={() => this.onPressEmoji(emoji)}
                            activeOpacity={0.7}
                            > 
                                <Image source={emoji} style={imgChild} />
                            </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        );
    }
}
