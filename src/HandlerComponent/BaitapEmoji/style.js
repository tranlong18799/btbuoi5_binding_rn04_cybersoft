import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textLg:{
        fontSize: 24,
        lineHeight: 32,
        color:'black'
    },
    imgMain:{
        marginVertical:30,
        height:150,
        width:150
    },
    flexRow:{
        flexDirection:'row'
    },
    imgChild:{
        height:50,
        width:50,
        margin:4
    }
});
